So how do the Zeds have their health scaled?

Doing a quick search in the source directories suggest the following:

    derpdev:user@derpd:~/KF2/official/KFGame/Classes$ !2039
    ag NumPlayersScale_HeadHealth
    KFMonsterDifficultyInfo.uc
    63:var const float NumPlayersScale_HeadHealth;
    67:var const float NumPlayersScale_HeadHealth_Versus;

    KFGameDifficultyInfo.uc
    164:            HeadHealthMod *= 1.0 + (GetNumPlayersHealthMod( NumLivingPlayers, P.DifficultySettings.default.NumPlayersScale_HeadHealth ));
    178:            HeadHealthMod *= 1.0 + (GetNumPlayersHealthMod( NumLivingPlayers, P.DifficultySettings.default.NumPlayersScale_HeadHealth_Versus ));

We are looking for a quick way to scale the health, so what is the best way?

As the numplayersscale value appears to be a const, I guess we cannot dynamically modify that.

So what about looking at the KFGameDifficultyInfo.uc?

    /** Scale health by the number of players.  This is applied as a bonus (zero is valid) for each player behind one */
    function float GetNumPlayersHealthMod( byte NumLivingPlayers, float HealthScale )
    {
        local float StartingLerp, LerpRate;

        // Early out if we have no living players
        if( NumLivingPlayers <= 0 )
        {
            return 0;
        }

        /** Scale the body health based on the number of players */
        if ( NumLivingPlayers <= `KF_MAX_PLAYERS )
        {
            return (NumLivingPlayers - 1) * HealthScale;
        }
        /** Slowly scale the additional health health to a max value if we are playing with more than 6 players */
        else
        {
            StartingLerp = HealthScale * `KF_MAX_PLAYERS;
            LerpRate = (NumLivingPlayers - `KF_MAX_PLAYERS) / OverMaxPlayers_MaxLerpedPlayers;
            return Lerp( StartingLerp, OverMaxPlayers_MaxHealthMod, fmin(LerpRate, 1.0f) );
        }
    }

Well that looks promising.

Who calls that function anyways? Turns out it is in the same class:

    /** Scales the health this Zed has by the difficulty level */
    function GetAIHealthModifier(KFPawn_Monster P, float GameDifficulty, byte NumLivingPlayers, out float HealthMod, out float HeadHealthMod, optional bool bApplyDifficultyScaling=true)
    {
        if ( P != none )
        {
            // Global mod * character mod
            if( bApplyDifficultyScaling )
            {
                HealthMod = GetGlobalHealthMod() * GetCharHealthModDifficulty(P, GameDifficulty);
                HeadHealthMod = GetGlobalHealthMod() * GetCharHeadHealthModDifficulty(P, GameDifficulty);
            }

            // invalid scaling?
            if ( HealthMod <= 0 )
            {
                HealthMod = 1.f;
                if( HeadHealthMod <= 0 )
                {
                    HeadHealthMod = 1.f;
                }
                return;
            }

            // Add another multiplier based on the number of players and the zeds character info scalers
            HealthMod *= 1.0 + (GetNumPlayersHealthMod( NumLivingPlayers, P.DifficultySettings.default.NumPlayersScale_BodyHealth ));
            HeadHealthMod *= 1.0 + (GetNumPlayersHealthMod( NumLivingPlayers, P.DifficultySettings.default.NumPlayersScale_HeadHealth ));
        }
    }

    /** Scales the health this Player (versus mode) Zed has by number of human players */
    function GetVersusHealthModifier(KFPawn_Monster P, byte NumLivingPlayers, out float HealthMod, out float HeadHealthMod)
    {
        if ( P != none )
        {
            HealthMod = GetGlobalHealthMod();
            HeadHealthMod = GetGlobalHealthMod();

            // Add another multiplier based on the number of players and the zeds character info scalers
            HealthMod *= 1.0 + (GetNumPlayersHealthMod( NumLivingPlayers, P.DifficultySettings.default.NumPlayersScale_BodyHealth_Versus ));
            HeadHealthMod *= 1.0 + (GetNumPlayersHealthMod( NumLivingPlayers, P.DifficultySettings.default.NumPlayersScale_HeadHealth_Versus ));
        }
    }

So it affects both Survival and VS Zeds

We generally only care about the Survival Zeds so where does 'GetAIHealthModifier' get used?

    derpdev:user@derpd:~/KF2/official/KFGame/Classes$ ag GetAIHealthModifier
    KFPawn_Monster.uc
    3328:        KFGI.DifficultyInfo.GetAIHealthModifier(self, KFGI.GameDifficulty, KFGI.GetLivingPlayerCount(), HealthMod, HeadHealthMod);
    3740:        KFGI.DifficultyInfo.GetAIHealthModifier(self, KFGI.GameDifficulty, KFGI.GetLivingPlayerCount(), HealthMod, HeadHealthMod);

    KFPawn_ZedHansBase.uc
    503:                KFGI.DifficultyInfo.GetAIHealthModifier(

    KFGameDifficultyInfo.uc
    140:function GetAIHealthModifier(KFPawn_Monster P, float GameDifficulty, byte NumLivingPlayers, out float HealthMod, out float HeadHealthMod, optional bool bApplyDifficultyScaling=true)

    KFGameInfo.uc
    1470:           DifficultyInfo.GetAIHealthModifier(P, GameDifficulty, LivingPlayerCount, HealthMod, HeadHealthMod);

Huh, what is KFGameInfo doing in there?

Having a look:

    /************************************************************************************
     * @name        Difficulty Scaling
     ***********************************************************************************/

    /** Adjusts AI pawn default settings by game difficulty and player count */
    function SetMonsterDefaults( KFPawn_Monster P )
    {
        local float HealthMod;
        local float HeadHealthMod;
        local float TotalSpeedMod, StartingSpeedMod;
        local float DamageMod;
        local int LivingPlayerCount;

        LivingPlayerCount = GetLivingPlayerCount();

        DamageMod = 1.0;
        HealthMod = 1.0;
        HeadHealthMod = 1.0;~~~~

        // Scale health and damage by game conductor values for versus zeds
        if( P.bVersusZed )
        {
            DifficultyInfo.GetVersusHealthModifier(P, LivingPlayerCount, HealthMod, HeadHealthMod);

            HealthMod *= GameConductor.CurrentVersusZedHealthMod;
            HeadHealthMod *= GameConductor.CurrentVersusZedHealthMod;

            // scale damage
            P.DifficultyDamageMod = DamageMod * GameConductor.CurrentVersusZedDamageMod;

            StartingSpeedMod = 1.f;
            TotalSpeedMod = 1.f;
        }
        else
        {
            DifficultyInfo.GetAIHealthModifier(P, GameDifficulty, LivingPlayerCount, HealthMod, HeadHealthMod);
            DamageMod = DifficultyInfo.GetAIDamageModifier(P, GameDifficulty,bOnePlayerAtStart);

            // scale damage
            P.DifficultyDamageMod = DamageMod;

            StartingSpeedMod = DifficultyInfo.GetAISpeedMod(P, GameDifficulty);
            TotalSpeedMod = GameConductor.CurrentAIMovementSpeedMod * StartingSpeedMod;
        }

        //`log("Start P.GroundSpeed = "$P.GroundSpeed$" GroundSpeedMod = "$GroundSpeedMod$" percent of default = "$(P.default.GroundSpeed * GroundSpeedMod)/P.default.GroundSpeed$" RandomSpeedMod= "$RandomSpeedMod);

        // scale movement speed
        P.GroundSpeed = P.default.GroundSpeed * TotalSpeedMod;
        P.SprintSpeed = P.default.SprintSpeed * TotalSpeedMod;

        // Store the difficulty adjusted ground speed to restore if we change it elsewhere
        P.NormalGroundSpeed = P.GroundSpeed;
        P.NormalSprintSpeed = P.SprintSpeed;
        P.InitialGroundSpeedModifier = StartingSpeedMod;

        //`log(P$" GroundSpeed = "$P.GroundSpeed$" P.NormalGroundSpeed = "$P.NormalGroundSpeed);

        // Scale health by difficulty
        P.Health = P.default.Health * HealthMod;
        if( P.default.HealthMax == 0 )
        {
            P.HealthMax = P.default.Health * HealthMod;
        }
        else
        {
            P.HealthMax = P.default.HealthMax * HealthMod;
        }

        P.ApplySpecialZoneHealthMod(HeadHealthMod);
        P.GameResistancePct = DifficultyInfo.GetDamageResistanceModifier(LivingPlayerCount);

        // debug logging
        `log("==== SetMonsterDefaults for pawn: " @P @"====",bLogAIDefaults);
        `log("HealthMod: " @HealthMod @ "Original Health: " @P.default.Health @" Final Health = " @P.Health, bLogAIDefaults);
        `log("HeadHealthMod: " @HeadHealthMod @ "Original Head Health: " @P.default.HitZones[HZI_HEAD].GoreHealth @" Final Head Health = " @P.HitZones[HZI_HEAD].GoreHealth, bLogAIDefaults);
        `log("GroundSpeedMod: " @TotalSpeedMod @" Final Ground Speed = " @P.GroundSpeed, bLogAIDefaults);
        //`log("HiddenSpeedMod: " @HiddenSpeedMod @" Final Hidden Speed = " @P.HiddenGroundSpeed, bLogAIDefaults);
        `log("SprintSpeedMod: " @TotalSpeedMod @" Final Sprint Speed = " @P.SprintSpeed, bLogAIDefaults);
        `log("DamageMod: " @DamageMod @" Final Melee Damage = " @P.MeleeAttackHelper.BaseDamage * DamageMod, bLogAIDefaults);
        //`log("bCanSprint: " @P.bCanSprint @ " from SprintChance: " @SprintChance, bLogAIDefaults);
    }

And finally where does this function get called?

    derpdev:user@derpd:~/KF2/official/KFGame/Classes$ ag SetMonsterDefaults
    KFPawn_Monster.uc
    659:    KFGameInfo(WorldInfo.Game).SetMonsterDefaults( self );

    KFPawn_MonsterBoss.uc
    65:/** The final sprint speed after being modified by SetMonsterDefaults() */

    KFCheatManager.uc
    4834:           KFGameInfo(WorldInfo.Game).SetMonsterDefaults( KFPawn_Monster(KFP));

    KFGameInfo.uc
    1440:function SetMonsterDefaults( KFPawn_Monster P )
    1508:           `log("==== SetMonsterDefaults for pawn: " @P @"====",bLogAIDefaults);

Cool, goes into KFPawn_Monster.uc, having a look at that:

Well lookee here:

    /** Called from Possessed event when this controller has taken control of a Pawn */
    function PossessedBy( Controller C, bool bVehicleTransition )
    {
        local string NPCName;
        local KFPlayerReplicationInfo KFPRI;
        local KFGameReplicationInfo KFGRI;

        Super.PossessedBy( C, bVehicleTransition );

        /** Set MyKFAIC for convenience to avoid casting */
        if( KFAIController(C) != none )
        {
            MyKFAIC = KFAIController( C );
        }

        bReducedZedOnZedPinchPointCollisionStateActive = false;
        //CollisionRadiusBeforeReducedZedOnZedPinchPointCollisionState = CollisionCylinderReducedPercentForSameTeamCollision;

        // Turn off air control for AI because it can mess up landing.
    `if(`notdefined(ShippingPC))
        if( IsHumanControlled() || MyKFAIC.bIsSimulatedPlayerController )
    `else
        if( IsHumanControlled() )
    `endif
        {
            KFPRI = KFPlayerReplicationInfo(C.PlayerReplicationInfo);
            if(KFPRI != none)
            {
                KFPRI.PlayerHealth  = Health;
                KFPRI.PlayerHealthPercent = FloatToByte( float(Health) / float(HealthMax) );
                SetCharacterArch(CharacterMonsterArch, true);
            }

            if( Role == ROLE_Authority )
            {
                LastAttackHumanWarningTime = WorldInfo.TimeSeconds;
            }
        }
        else
        {
            AirControl = AIAirControl;
        }

        KFGameInfo(WorldInfo.Game).SetMonsterDefaults( self );

        if( MyKFAIC != none && MyKFAIC.PlayerReplicationInfo != None )
        {
            NPCName = string(self);
            NPCName = Repl(NPCName,"KFPawn_Zed","",false);
            PlayerReplicationInfo.PlayerName = NPCName;
            MyKFAIC.PlayerReplicationInfo.PlayerName = NPCName;
        }

        // Set our (Network: Server) difficulty-based settings
        KFGRI = KFGameReplicationInfo( WorldInfo.GRI );
        if( KFGRI != none )
        {
            SetBlockSettings( DifficultySettings.static.GetBlockSettings(self, KFGRI) );

            // Set any AI-specific difficulty settings
            if( MyKFAIC != none )
            {
                MyKFAIC.EvadeOnDamageSettings = DifficultySettings.static.GetEvadeOnDamageSettings( self, KFGRI );
            }
        }

        // Set audio switch based on AI or player-controlled
    `if(`notdefined(ShippingPC))
        SetSwitch( 'Player_Zed', (IsHumanControlled() || (MyKFAIC != none && MyKFAIC.bIsSimulatedPlayerController)) ? 'Player' : 'NotPlayer' );
    `else
        SetSwitch( 'Player_Zed', IsHumanControlled() ? 'Player' : 'NotPlayer' );

    `endif
    }

So this seems to connect the dots back from changing the function GetNumPlayersHealthMod to the health of a Zed.

##################################################
## Scaling Boss health
##################################################

How to scale boss health?

Seems like the bosses are based off of KFGame.KFPawn_MonsterBoss

Where does that show up?

    KFSM_Patriarch_Grapple.uc
    76:    KFPawn_MonsterBoss(KFPOwner).PlayGrabDialog();
    159:        KFPawn_MonsterBoss(KFPOwner).PlayGrabbedPlayerDialog( KFPawn_Human(Follower) );

    KFSM_Zed_Boss_Theatrics.uc
    82:     local KFPawn_MonsterBoss KFBoss;
    93:             KFBoss = KFPawn_MonsterBoss(KFPOwner);
    114:    local KFPawn_MonsterBoss BossPawn;
    151:    BossPawn = KFPawn_MonsterBoss( KFPOwner );
    223:    local KFPawn_MonsterBoss BossPawn;
    225:    BossPawn = KFPawn_MonsterBoss( KFPOwner );

    KFPawn_MonsterBoss.uc
    2:// KFPawn_MonsterBoss
    10:class KFPawn_MonsterBoss extends KFPawn_Monster

    KFPawn_ZedHansBase.uc
    12:class KFPawn_ZedHansBase extends KFPawn_MonsterBoss

    KFPlayerController.uc
    2772:   local KFPawn_MonsterBoss KFBoss;
    2791:           if( !ViewTarget.IsA('KFPawn_MonsterBoss') )
    2863:function KFPawn_MonsterBoss GetBoss()
    2865:   local KFPawn_MonsterBoss KFBoss;
    2867:   foreach WorldInfo.AllPawns( class'KFPawn_MonsterBoss', KFBoss )
    4816:function ShowBossNameplate( KFPawn_MonsterBoss KFBoss, optional string PlayerName)
    7952:           if( PlayerCamera.CameraStyle == 'Boss' && KFPawn_MonsterBoss(NewViewTarget) != none )

    KFGFxWidget_BossHealthBar.uc
    15:var KFPawn_MonsterBoss BossPawn;
    56:function SetBossPawn(KFPawn_MonsterBoss NewBossPawn)

    KFBossCamera.uc
    26:var KFPawn_MonsterBoss ViewedPawn;
    70:             ViewedPawn = KFPawn_MonsterBoss(P);

    KFCheatManager.uc
    3425:    local KFPawn_MonsterBoss BossPawn;
    3429:        BossPawn = KFPawn_MonsterBoss(KFAIC.MyKFPawn);

    KFGFxHudWrapper.uc
    20:var KFPawn_MonsterBoss BossPawn;

Perhaps what makes sense is to adjust at the the gameinfo level?

Nope, it's too complicated. Probably best to adjust one level down.

It seems GetNumPlayersHealthMod is only called by GetAIHealthModifier so this means we can modify how this gets setup.

Modify GetAIHealthModifier so it provides a custom GetNumPlayersHealthMod the pawn type and adjust health scaling keyed on that.


##################################################
## ControlledDifficulty Modifications
##################################################

Now, if this change were to work well with CD, how would that happen?

Does CD override this? Nope. Does not touch it.

It does, however, extend KFDifficultyInfo: CD_DifficultyInfo.uc

And for that to work, it also loads CD_DifficultyInfo into the GameInfo via: "CD_Survival.uc"

What about the chat commands? That code also seems to be stored within CD_Survival

So a new !cd command, !cdzedhealthscale
And a new configuration ZedHealthScale

Call the module ZedHealthScale

So after looking at CD, it appears that the chat based commands are behind variables declared private.

So building a personal version may be necessary. :(



