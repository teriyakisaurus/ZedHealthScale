class ZHS_Survival extends CD_Survival;

`include(ControlledDifficulty/CD_Log.uci)

var config byte ZedHealthScale;
var config byte BossHealthScale;

function SetZedHealthScale(byte ScaleAmount)
{
    ZedHealthScale = ScaleAmount;
    ZHS_DifficultyInfo(DifficultyInfo).ZedHealthScale = ScaleAmount;
}

function SetBossHealthScale(byte ScaleAmount)
{
    BossHealthScale = ScaleAmount;
    ZHS_DifficultyInfo(DifficultyInfo).BossHealthScale = ScaleAmount;
}

event InitGame( string Options, out string ErrorMessage )
{
    local byte ZedHealthScaleFromGameOptions;

    Super.InitGame(Options,ErrorMessage);

    if ( HasOption(Options, "ZedHealthScale") )
    {
        ZedHealthScaleFromGameOptions = byte(GetIntOption( Options, "ZedHealthScale", 0 ));
        `cdlog("ZedHealthScale= "$ZedHealthScaleFromGameOptions$" (0.0=missing)", bLogControlledDifficulty);
        SetZedHealthScale(ZedHealthScaleFromGameOptions);
    }
}


event Broadcast (Actor Sender, coerce string Msg, optional name Type)
{
    super.Broadcast(Sender, Msg, Type);

    // We don't do anything if it's not a chat command
    if ( Type != 'Say' )
    {
        return;
    }

    RunChatCommandIfAuthorized(Sender,Msg);
}


function RunChatCommandIfAuthorized( Actor Sender, string CommandString )
{
    local string ResponseMessage;
    local array<string> CommandTokens;

    // Pinched from ControlledDifficulty (can't override CD's as it's private)
    // Check if the command starts with !zhs
    if ( 4 > Len( CommandString ) || !( Left( CommandString, 4 ) ~= "!zhs" ) )
    {
        return;
    }

    // Chat commands are case-insensitive.  Lowercase the command now
    // so that we can do safely do string comparisons with lowercase
    // operands below.
    CommandString = Locs( CommandString );

    // Split the chat command on spaces, dropping empty parts.
    ParseStringIntoArray( CommandString, CommandTokens, " ", true );

    ResponseMessage = "Unknown Command";
    ResponseMessage = CommandTokens[0];

    if ( CommandTokens[0] == "!zhsb" ) {
        if ( CommandTokens.Length == 2 ) {
            SetBossHealthScale(byte(CommandTokens[1]));
        }
        if ( ZedHealthScale == 0 )
        {
            ResponseMessage = "BossHealthScale: Scaling to current number of players";
        }
        else {
            ResponseMessage = "BossHealthScale: "$BossHealthScale$"P equivalent";
        }
    }

    else if ( CommandTokens[0] == "!zhs" ) {
        if ( CommandTokens.Length == 2 ) {
            SetZedHealthScale(byte(CommandTokens[1]));
        }
        if ( ZedHealthScale == 0 )
        {
            ResponseMessage = "ZedHealthScale: Scaling to current number of players";
        }
        else {
            ResponseMessage = "ZedHealthScale: "$ZedHealthScale$"P equivalent";
        }
    }

    // An authorized command match was found; the command may or may not
    // have succeeded, but something was executed and a chat reply should
    // be sent to all connected clients
    super.Broadcast(None, ResponseMessage, 'CDEcho');
}

defaultproperties
{
    DifficultyInfoClass=class'ZedHealthScale.ZHS_DifficultyInfo'
}
